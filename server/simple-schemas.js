export function objectSize(obj) {
  let inc=0;
  for(key in obj) inc++;
  return inc;
}

export const schemaUserKeys = new SimpleSchema({
  _id: {
    type: String,
  },
  publicKey: {
    type: String,
     min: 300,//tested with 440
     max: 512,
  },
  privateKey: {
    type: String,
    min: 2000, //tested with 2200
    max: 2600,
  },
  encrypted: {
    type: Boolean,
  }
});
export const schemaGroupUsers = new SimpleSchema({
  _id: {
    type: String,
  },
  passwords: {
    type: Object,
    blackbox: true,
    //TODO: Validate for all UserId:String
  },
  groupAdmins: {
    type: [String],
  },
  groupType: {
    type: String,
    optional: true,
  },
  encrypted: {
    type: Boolean,
  }
});

SGU = schemaGroupUsers;
