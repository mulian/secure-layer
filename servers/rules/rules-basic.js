export default
class RulesBasic {
  constructor(ownColName="owner") {
    this.ownColName = ownColName;
  }

  getObj() {
    var _this = this;
    return {
      insert: function (...args) { return _this.insert.apply(_this,args) },
      update: function (...args) { return _this.update.apply(_this,args) },
      remove: function (...args) { return _this.remove.apply(_this,args) },
    }
  }
  isMyDoc(userId,doc) {
    if(userId) {
      if(doc[this.ownColName] instanceof Array)
        return _.contains(doc[this.ownColName],userId);
      else return doc[this.ownColName]==userId;
    }
  }
  isSetUpdated(modifier) {
    return modifier.$set && modifier.$set.updated && Match.test(modifier.$set.updated,Date);
  }
  isSetOnlyUpdated(modifier) {
    return this.isSetUpdated(modifier) && _.size(modifier.$set)==1;
  }

  onlyAddToSetAndUpdated(modifier) {
    return modifier.$addToSet && _.size(modifier)==2 && this.isSetOnlyUpdated(modifier);
  }
  onlySet(modifier) {
    return modifier.$set && _.size(modifier)==1;
  }
}
