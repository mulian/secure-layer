/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */
import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import { DDP } from 'meteor/ddp-client';
// import { FlowRouter } from 'meteor/kadira:flow-router';
import { assert } from 'meteor/practicalmeteor:chai';
import { chai, expect } from 'meteor/practicalmeteor:chai';
import { Promise } from 'meteor/promise';
import { $ } from 'meteor/jquery';

// import {dbUserKeys,dbGroupsUsers} from '../secure-layer.js'

import {dbUserKeys,dbGroupsUsers} from "meteor/mulian:secure-layer";

import {createUser,subscribers,Users,defaultCheckCollectionsData} from './secure-layer-test-tools.js'
import SecureCollectionTest from './secure-collection-test.js'


if(Meteor.isServer) {
  let sCollection = new SecureCollectionTest();

  Meteor.publish('db_secure_collection_test', function() {
    return sCollection.find(SecureLayer.tools.getQuery(this.userId));
  });
  Meteor.methods({
    'user.remove'(id) {
      Meteor.users.remove({_id:id});
    },
    'resetCollection'() {
      sCollection.remove({});
      dbUserKeys.remove({});
      dbGroupsUsers.remove({});
    }
  });
}
function clientFlash() {
  localStorage.clear();
  sessionStorage.clear();
}

var collection;
describe('SecureLayer', function () {
  if(Meteor.isClient) {
    var newValue;
    before(function() {
      collection = new SecureCollectionTest();
      clientFlash();
      subscribers();
    });

    describe('Create Users',function() {
      it('<user1>',function(done) {
        this.timeout(10000);
        createUser('user1',done);
      });
    });
    describe('Create Users',function() {
      it('<user2>',function(done) {
        this.timeout(10000);
        createUser('user2',done);
      });
    });
    describe('Create Users',function() {
      it('<user3>',function(done) {
        this.timeout(10000);
        createUser('user3',done);
      });
    });

    it('No login, SecureLayer can not be reached.', function () {
      assert.equal(Meteor.userId(),null);
      assert.equal(SecureLayer.user.getPassword(),undefined);
      // console.log(SecureLayer.user.isLoggedIn());
      // assert.throw(SecureLayer.user.isLoggedIn(),"User not logged in, you can't use SecureLayer!");
    });


    describe('Login Test with <user1>', function () {
      before(function(done) {
        this.timeout(6000);
        createUser('user1',done);
      });

      it('User <user1> was created successsfully.', function () {
        assert.notEqual(Meteor.userId(),undefined);
        assert.notEqual(SecureLayer.user.getPassword(),undefined);
      });

      describe('Use secured data collection.', function () {
        var user1insertId,user1group;
        before(function() {
          // collection = new SecureCollectionTest();
        });

        it("Can't create groups with incorrect userIds (collection.createGroup).", function() {
          assert.equal(collection.createGroup(['xx']),false);
          assert.equal(collection.createGroup([Meteor.userId(),'xx']),false);
        });
        it('Automatically add your own userId on collection.createGroup.', function() {
          assert.notEqual(collection.createGroup(),false);
          //TODO get password do verify that im in
          //TODO with another user
        });
        it('Collection insert without- or with wrong groupId trigger error.', function() {
          assert.throw(function() {
            collection.insert({nonSecure:'no Secure Attribute1',secure1:'Secure Attribute 1'})
          },/doc\.groupId must be set/);
          assert.throw(function() { //wrong groupId
            collection.insert({nonSecure:'no Secure Attribute2',secure1:'Secure Attribute 1',groupId:'1a'})
          },/doc\.groupId.*/);
        });
        it('Create groupId (with type), insert with groupId, check secure and unsecure attributes, check (after-)transform', function(done) {
          this.timeout(4000);
          let typeValue = 'test';
          collection.createGroup([Meteor.userId()],{
            type: typeValue,
            callback:function(err,id) {
              user1group = id;
              if(!err) {
                assert.equal(dbGroupsUsers.findOne({_id:id}).groupType,typeValue);
                collection.insert({nonSecure:'no Secure Attribute3',secure1:'Secure Attribute 2',groupId:user1group},function(err1,id) {
                  if(!err1) {
                    user1insertId = id;
                    defaultCheckCollectionsData(collection,user1insertId);
                    done();
                  }
                });
              }
            },
          });
        });

        describe('Login Test with <user2>', function () {
          var contentId,content;
          before(function(done) {
            this.timeout(6000);
            createUser('user2',done);
          });

          it('User <user2> was created successsfully.', function () {
            assert.notEqual(Meteor.userId(),undefined);
            assert.notEqual(SecureLayer.user.getPassword(),undefined);
            assert.notEqual(Meteor.userId(),Users.user1._id);
          });
          it('Create GroupID with <user1> (auto add logged in <user2>)', function() {
            assert.notEqual(collection.createGroup([Users.user1._id]),false);
            //TODO Test if group is with booth
          });
          it('User <user2>: insert Data with <user1>.', function (done) {
            collection.createGroup([Users.user1._id],{
              callback:function(err,groupId) {
                content = {nonSecure:'no Secure Attribute4',secure1:'Secure Attribute 3',groupId:groupId};
                contentInsert = {nonSecure:'no Secure Attribute4',secure1:'Secure Attribute 3',groupId:groupId};
                //should not be the same, inserted will be encrypted
                if(!err) {
                  collection.insert(contentInsert,function(err1,id) {
                    assert.equal(err1,undefined);
                    if(!err1) {
                      contentId = id;
                      defaultCheckCollectionsData(collection,id);

                      done();
                    }
                  });
                }
              }
            });
          });
          it('User <user2> insert with local Collection.', function (done) {
            let contentInsert2 = {nonSecure:'no Secure Attribute5',secure1:'asd!2',groupId:content.groupId};
            //should not be the same, inserted will be encrypted
            collection.local.insert(contentInsert2,function(err1,id) {
              assert.equal(err1,undefined);
              if(!err1) {
                let collections = defaultCheckCollectionsData(collection,id);

                done();
              }
            });
          });
          it("User <user2> can't updates <user1> inserted doc with local Collection (<user2> is not in group).", function () {
            assert.throw(function() {
              collection.local.update({_id:user1insertId},{$set:{secure1:'new!!'}});
            },"There is no Doc to update.");
          });

          describe('Update, must be a describe to sync with preview it.', function () { //must be describe, to sync pref. it
            it('User <user1> update <user2> inserted doc with local Collection.', function () {
              newValue = "new Value!"
              collection.update({_id:contentId},{$set:{secure1:newValue}});

              let collections = defaultCheckCollectionsData(collection,contentId);
              assert.equal(collections.decrypted.secure1,newValue);
            });
          });

          describe('Login Test with <user3>.', function () {
            before(function(done) {
              this.timeout(6000);
              createUser('user3',done);
            });

            it('User <user3> was created successsfully.', function () {
              assert.notEqual(Meteor.userId(),undefined);
              assert.notEqual(SecureLayer.user.getPassword(),undefined);
              assert.notEqual(Meteor.userId(),Users.user1._id);
              assert.notEqual(Meteor.userId(),Users.user2._id);
            });
            it("User <user3> could't read <user2&1> group content.", function () {
              let obj = collection.findOne({_id:contentId});
              assert.equal(obj,undefined);
            });
            it("User <user3> could't read <user1> group content", function () {
              let obj = collection.findOne({_id:user1insertId});
              assert.equal(obj,undefined);
            });
          });
          describe('Login Test with <user1> again.', function () {
            before(function(done) {
              this.timeout(6000);
              createUser('user1',done);
            });

            it('User <user1> was logged in successsfully.', function () {
              assert.notEqual(Meteor.userId(),undefined);
              assert.notEqual(SecureLayer.user.getPassword(),undefined);
              assert.equal(Meteor.userId(),Users.user1._id);
            });
            it('User <user1> could read <user2> group content. (They are booth in this group.)', function () {
              let collections = defaultCheckCollectionsData(collection,contentId);
              assert.equal(collections.decrypted.nonSecure,content.nonSecure);
              assert.equal(collections.decrypted.secure1,newValue);
            });
            it("Change <user1> group, add <user3>.",function(done) {
              console.log(this.test.title);
              this.timeout(10000);
              let obj = collection.findOne({_id:user1insertId});
              console.log("here",obj);
              assert.equal(_.contains(obj.groupUsers,Users.user1._id),true);
              assert.equal(_.contains(obj.groupUsers,Users.user3._id),false);
              collection.alterUsers(user1group,[Users.user3._id]);
              setTimeout(function() {
                let doc = collection.findOne({_id:user1insertId});
                console.log(doc);
                console.log("jo");
                assert.equal(_.contains(doc.groupUsers,Users.user1._id),true);
                assert.equal(_.contains(doc.groupUsers,Users.user3._id),true);
                done();
              },1000);
              // assert.equal(_.contains(obj.users,Users.user3._id),true);
              //TODO check
            });

            describe('Login Test with <user3>.', function () {
              before(function(done) {
                this.timeout(6000);
                createUser('user3',done);
              });

              it('User <user3> was logged in successsfully.', function () {
                assert.notEqual(Meteor.userId(),undefined);
                assert.notEqual(SecureLayer.user.getPassword(),undefined);
                assert.notEqual(Meteor.userId(),Users.user1._id);
                assert.notEqual(Meteor.userId(),Users.user2._id);
              });
              it("User <user3> could read <user1> group content", function () {
                let collections = defaultCheckCollectionsData(collection,user1insertId);
              });
              it("User <user3> try to add <user2> to group.", function (done) {
                this.timeout(10000);
                collection.alterAdmins(user1group,[Users.user1._id,Users.user2._id,Users.user3._id]);
                setTimeout(function() {
                  let doc = collection.findOne({_id:user1insertId});
                  console.log(doc);
                  assert.equal(_.contains(doc.groupAdmins,Users.user1._id),true);
                  assert.equal(_.contains(doc.groupAdmins,Users.user2._id),false);
                  assert.equal(_.contains(doc.groupAdmins,Users.user3._id),false);
                  done();
                },500);
              });
            });
          });
        });
      });
      after(function() {
        clientFlash();
        Meteor.call('user.remove',Users.user1._id);
        Meteor.call('user.remove',Users.user2._id);
        Meteor.call('user.remove',Users.user3._id); //in comment for reuse user test
        Meteor.call('resetCollection');
        console.log("SecureLayer DONE");
      });
    });
  } else if(Meteor.isServer) { //Server side
    before(function() {
    });
    describe('Data encrypted test', function () {
      it('Are the data in the group encrypted?', function () {

      });
    });
    after(function() {
      console.log("SecureLayer DONE");
    });
  }
  describe('Tools test', function () {
    it('Create random id.', function () {
      assert.equal(SecureLayer.tools.randId()==SecureLayer.tools.randId(),false);
      assert.equal(SecureLayer.tools.randId(10).length,10);
      assert.equal(SecureLayer.tools.randId(10)==SecureLayer.tools.randId(10),false);
    });
  });
},function() {
  describe('Data encrypted test', function () {
    it('Are the data in the group encrypted?', function () {

    });
  });
  describe('Tools test', function () {
    it('Create random id.', function () {
      assert.equal(SecureLayer.tools.randId()==SecureLayer.tools.randId(),false);
      assert.equal(SecureLayer.tools.randId(10).length,10);
      assert.equal(SecureLayer.tools.randId(10)==SecureLayer.tools.randId(10),false);
    });
  });
});
