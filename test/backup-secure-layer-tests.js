// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by secure-layer.js.
import { name as packageName } from "meteor/mulian:secure-layer";

// Write your tests here!
// Here is an example.
Tinytest.add('secure-layer - example', function (test) {
  test.equal(packageName, "secure-layer");
});
