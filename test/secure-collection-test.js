// import DBSecureCollection from '../db-secure-collection.js'
import {dbUserKeys,dbGroupsUsers} from "meteor/mulian:secure-layer";

export default
class SecureCollectionTest extends SecureLayer.SecureCollection {
  constructor() {
    super('secure-collection-test',['secure1','secure2'],{
      transform: function(doc) {
        doc.transform1 = true;
        return doc;
      },
      localTransform: function(doc) {
        doc.transform2 = true;
        return doc;
      },
    });
    this.allow({
      insert: function(userId,doc) {
        return true;
      },
      update: function(userId,doc,fields,modifier) {
        return true;
      },
      remove: function(userId,doc) {
        return true;
      },
    });
  }
}
