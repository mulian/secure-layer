// export const name = 'secure-layer'; //??
"use strict";
import DBUserKeys from '../booth/db-user-keys.js'
import DBGroupsUsers from '../booth/db-groups-users.js'
import DBSecureCollection from '../booth/db-secure-collection.js'

const plainDBUserKeys = new DBUserKeys();
const plainDBGroupsUsers = new DBGroupsUsers();
export const dbUserKeys = plainDBUserKeys;
export const dbGroupsUsers = plainDBGroupsUsers;
export const SecureCollection = DBSecureCollection;

const cachList = new Mongo.Collection(null);

var collections = [];
export function addCollection(collection) {
  collections.push(collection);
}

// if(Meteor.isClient) {
//   DBUK = dbUserKeys;
//   DBGU = dbGroupsUsers;
// }

var logoutExecuted=false;
function onLogout() {
  for(coll of collections) {
    coll.local.disconnect();
  }
  logoutExecuted=true;
  delete plainDBUserKeys.privateKey; //remove PrivateKey catch
  localStorage.removeItem('hash'); //remove LocalStoarage Hash
  cachList.remove({}); //clear cach
}
function onLogin() {
  if(logoutExecuted) {
    for(coll of collections) {
      coll.local.connect();
    }
  }
};

Meteor.startup(function () {
  if(Meteor.isClient) {
    Meteor.autorun(function () {
      if (Meteor.userId()) {
        onLogin();
      } else {
        onLogout();
      }
    });
  }
});

function cache(key,calcValue) {
  let value, doc = cachList.findOne({key:key});
  if(doc==undefined) {
    value = calcValue()
    cachList.insert({
      key: key,
      value: value,
    })
  } else value = doc.value;
  return value;
}
SecureLayer = {
  SecureCollection: SecureCollection,
  DBUserKey: dbUserKeys,
  DBGroupsUsers: dbGroupsUsers,
  user: {
    login: function(username,password,callback) {
      Meteor.loginWithPassword(username,password, function(err) {
        if(!err) SecureLayer.user.setPassword(password);
        callback(err);
      });
    },
    setPassword: function(password) {
      return localStorage.setItem('hash',SecureLayer.crypt.MD5(password));
    },
    changePassword: function(newPassword) {
      return dbUserKeys.changePassword(newPassword);
    },
    getPassword: function() {
      pw = localStorage.getItem('hash');
      if(pw==undefined) Meteor.logout();
      return pw;
    },
    create: function(obj,callback) {
      if(obj.password) {
        SecureLayer.user.setPassword(obj.password);
        Accounts.createUser(obj,function(err) {
          if(err) callback(err);
          else return dbUserKeys.create(callback);
        });
      }
      return false;
    },
    isLoggedIn: function() {
      if(this.loggedIn==undefined && Meteor.userId()==undefined) throw "User not logged in, you can't use SecureLayer!";
      else this.loggedIn=true;
    }
  },
  crypt: {
    MD5 : function(toHashStr) {
      return CryptoJS.MD5(toHashStr).toString();
    },
    AES : {
      enc: function(text,pw) {
        let encKey = CryptoJS.AES.encrypt(text, pw);
        return encKey.toString(); //encText
      },
      dec: function(encText,pw) {
        if(encText==undefined || encText=="" || pw==false || pw==undefined) {
          throw {
            msg:"Error SecureLayer.AES.dec",
            encText:encText,
            pw: pw
          }
          return "";
        }
        else
          return cache('aes_'+encText+pw,function() {
            return CryptoJS.AES.decrypt(encText, pw).toString(CryptoJS.enc.Utf8);
          });
      }
    },
    RSA : {
      encFor: function(text,userId) {
        //userId == undefined -> own PublicKey
        let userPublicKey = SecureLayer.user.getPublicKey(userId);
        return SecureLayer.crypt.RSA.enc(text,userPublicKey);
      },
      enc: function(text,publicKey) {
        var crypt_public = new JSEncrypt({
            default_key_size: 2048
        });
        crypt_public.setKey(publicKey);
        // Encrypt the data with the public key.
        return crypt_public.encrypt(text); //encText
      },
      decFor: function(encText) {
        let myPrivateKey = SecureLayer.user.getPrivateKey();
        return SecureLayer.crypt.RSA.dec(encText,myPrivateKey);
      },
      dec: function(encText,privateKey) {
        return cache('rsa_'+encText+'_'+privateKey,function() {
          if(privateKey==undefined) privateKey = SecureLayer.user.getPrivateKey();
          let crypt_private = new JSEncrypt({
              default_key_size: 2048
          });
          crypt_private.setKey(privateKey);
          return crypt_private.decrypt(encText);
        })
      },
      createAsyncKeys: function() {
        crypt = new JSEncrypt({
            default_key_size: 2048
        });
        var dt = new Date();
        var time = -(dt.getTime());
        crypt.getKey();
        dt = new Date();
        time += (dt.getTime());

        return {
          private: crypt.getPrivateKey(),
          public: crypt.getPublicKey(),
        }
      }
    }
  },
  tools: {
   randId: function(length=32) {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for( var i=0; i < length; i++ )
          text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
    },
    getQuery: function(userID) {
      if(Meteor.isServer)
        return {$or: [
          {owner: userID},
          {groupUsers: userID},
        ]};
      else return null;
    }
  }
}

if(Meteor.isServer) {
  Meteor.publish('db_secure_user', function() {
    return dbUserKeys.find();
  });
  Meteor.publish('db_secure_groups_users', function() {
    let show={encrypted:1};
    show[this.userId]=1;
    let query={encrypted:true};
    query['passwords.'+this.userId]={$exists:true};
    return dbGroupsUsers.find(query,show);
  });
  Meteor.publish('db_users', function () {
    return Meteor.users.find({},{_id:1,username:1});
  });

  Meteor.methods({
    // 'dbGroupsUsers.alterAllGroupUsers'(groupId,userIds) {
    //   //TODO: check if user is in Group
    //   for(coll of collections) {
    //     coll.update({groupId:groupId},{$set:{groupUsers:userIds}},{multi:true});
    //   }
    //   return true;
    // },
    // 'dbGroupsUsers.alterGroupAdmins'(groupId,userIds) {
    //   //TODO: check if user is in Group
    //   console.log("jo");
    //   for(coll of collections) {
    //     coll.update({groupId:groupId},{$set:{groupAdmins:userIds}},{multi:true});
    //   }
    //   return true;
    // }
  })
  export function dbGroupsUsers_alterAllGroupUsers(groupId,passwords) {
    //TODO: check if user is in Group
    let users = [];
    for(key in passwords) {
      users.push(key)
    }

    for(coll of collections) {
      coll.update({groupId:groupId},{$set:{groupUsers:users}},{multi:true});
    }
    return true;
  }
  export function dbGroupsUsers_alterGroupAdmins(groupId,userIds) {
    //TODO: check if user is in Group
    // console.log("jo");
    for(coll of collections) {
      coll.update({groupId:groupId},{$set:{groupAdmins:userIds}},{multi:true});
    }
    return true;
  }
} else {
  // Meteor.methods({
  //   'dbGroupsUsers.add'(groupId,userId) {
  //
  //   }
  // })
}
