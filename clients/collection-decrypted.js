// "use strict";
//TODO: Rename to After transform or so
export default
class CollectionDecrypted extends Mongo.Collection {
  constructor(syncCollection,options) {
    super(null,options);
    this.syncCollection = syncCollection;

    this.remapDML();
    this.connect();
  }
  remapDML() {
    this.replaceOriginalWithSyncCollection('insert');
    this.replaceOriginalWithSyncCollection('update');
    this.replaceOriginalWithSyncCollection('remove');
  }
  replaceOriginalWithSyncCollection(functionName) {
    "use strict";
    var original = this[functionName];
    this[functionName] = (...args) => {
      this.syncCollection[functionName].apply(this.syncCollection,args);
    }
    this[functionName+'Original'] = original;
  }
  disconnect() {
    if(this.observerComputation) {
      this.observerComputation.stop();
      this.removeOriginal({});
      delete this.observerComputation;
    }
  }
  connect() {
    if(!this.observerComputation) {
      this.observerComputation = this.syncCollection.find().observe({
        added: (doc) => {
          // console.log('added',doc);
          // console.log("added");
          doc = this.syncCollection.decryptTransform(doc);
          // console.log(doc);
          this.insertOriginal(doc);
        },
        changed: (doc) => {
          this.updateOriginal({_id:doc._id},doc);
          // console.log('changed',doc);
        },
        removed: (doc) => {
          this.removeOriginal({_id:doc._id});
          // console.log('removed',doc);
        },
      });
    }
  }
}
