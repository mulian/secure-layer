import {dbGroupsUsers,dbGroups,dbUserKeys,dbGroupsUsers_alterGroupAdmins,dbGroupsUsers_alterAllGroupUsers}
  from '../clients/secure-layer.js'
import {schemaGroupUsers} from '../servers/simple-schemas.js'
/* Class DBGroupsUsers
* User of
*/
export default
class DBGroupsUsers extends Mongo.Collection {
  constructor(options) {
    super('secure_layer_groups_users',{
      transform: (doc) => {
        if(Meteor.userId() && Meteor.isClient) {
          doc.users = this._getGroupeUsers(doc);
          doc = this._decrypt(doc);
        }
        return doc;
      },
    });
    //TODO: able to use with
    this.checkAlterPasswords = function() {return true};
    this.checkAlterAdmins = function() {return true};
    if(Meteor.isServer)
      this.allow({
        insert: function(userId,doc) {
          return userId && Match.test(doc,schemaGroupUsers) && doc.passwords[userId]!=undefined;
        },
        update: (userId,doc,fields,modifier) => {
          // console.log(userId+" update group:", modifier);
          // console.log(userId,fields);
          // console.log("jo?");
          // console.log(doc);
          // console.log(modifier);
          // console.log(this.checkAlterPasswords());
          if(userId && _.contains(doc.groupAdmins,userId)) {
            if(modifier && _.size(modifier)==1 && modifier['$set']) {
              if(modifier.$set && _.size(modifier.$set)==1) {
                if(modifier.$set.passwords!=undefined) {
                  if(this.checkAlterPasswords(userId,doc,fields,modifier,modifier.$set.passwords)) {
                    dbGroupsUsers_alterAllGroupUsers(doc._id,modifier.$set.passwords);
                    // console.log("true!");
                    // console.log("jo ging!");
                    return true;
                  }
                } else if(modifier.$set.groupAdmins!=undefined) {
                  if(this.checkAlterAdmins(userId,doc,fields,modifier)) {
                    dbGroupsUsers_alterGroupAdmins(doc._id,modifier.$set.groupAdmins);
                    return true;
                  }
                }
              }
            }
          }
          return false;
        },
        remove: function(userId,doc) {
          //only if one is in Group and password for him is set
          if(_.size(doc.passwords)==1 && doc.passwords[userId]) return true;
          return false;
        },
      });

  }
  _decrypt(doc) {
    if(doc.encrypted) {
      let encryptedPw = doc.passwords[Meteor.userId()];
      if(encryptedPw) {
        doc.password = SecureLayer.crypt.RSA.dec(encryptedPw,dbUserKeys.getPrivateKey());
        if(doc.password!=null) doc.encrypted = false;
        delete doc.passwords;
      }
    }
    return doc;
  }

  _addMeIfImNotIn(userIds) {
    let imIn=false;
    for(userId of userIds)
      if(userId==Meteor.userId())
        imIn=true;
    if(imIn==false) userIds.push(Meteor.userId());
    return userIds;
  }
  _createUserPasswordObj(userIds,plainPW) {
    let obj = {};
    for(userId of userIds) {
      let publicPwFromUser = dbUserKeys.getPublicKey(userId);
      if(publicPwFromUser==undefined) {
        return undefined;
      }
      obj[userId] = SecureLayer.crypt.RSA.enc(plainPW,publicPwFromUser);
    }
    return obj;
  }
  /*
  * Creates encrypted password for an User with Group
  *
  * @param {string} userId default own Meteor User Id (without userId -> create new Group)
  * @param {object} options
  * @param {function} options.callback callback after insert
  * @param {string} option.type type of group
  * @return {boolean} false if there is a not exists UserId, else: groupId and PW
  */
  create(userIds=[],options={}) {
    userIds = this._addMeIfImNotIn(userIds); //todo remove duplicat
    let plainGroupPassword = SecureLayer.tools.randId(30);
    let insertObj = {
      passwords: this._createUserPasswordObj(userIds,plainGroupPassword),
      groupAdmins: [Meteor.userId()],
      encrypted: true,
      groupType: options.type,
    }
    if(insertObj.passwords==undefined) return false;
    return this.insert(insertObj,options.callback);
  }
  _get(id) {
    if(id instanceof Object) return id;
    else return this.findOne({_id:id});
  }
  getGroupsFrom(userIds,type) {
    if(!(userIds instanceof Array)) userIds = [userIds]
    let q = {};
    if(type) q.groupType = type;
    for(ids of userIds) {
      q["passwords."+ids] = {$exists:true};
    }
    return this.find(q);
  }
  //* TODO: Only from server how checks user Id is already in group
  /* Alter Users
  *
  * @param {number} groupId the group id.
  * @return {boolean} true means only that it was possible to trigger update
  */
  alterUsers(groupId,userIds=[],callback) {
    userIds = this._addMeIfImNotIn(userIds);
    let doc = this._get(groupId);
    if(doc) {
      let plainGroupPassword = doc.password;
      let obj = {
        passwords: this._createUserPasswordObj(userIds,plainGroupPassword),
      }
      if(obj.passwords) {
        this.update({_id:groupId},{$set:obj},callback);
        //TODO: transport to allow
        // Meteor.call('dbGroupsUsers.alterAllGroupUsers',groupId,userIds,function(err,result) {
        //   console.log(err,result);
        //   if(!err && callback) callback(result);
        // });
        return true;
      }
    }
    return false;
  }
  alterAdmins(groupId,userIds=[],callback) {
    userIds = this._addMeIfImNotIn(userIds);
    this.update({_id:groupId},{$set:{
      groupAdmins: userIds,
    }});
  }
  _getGroupeUsers(groupId) {
    let users = [];
    let doc = this._get(groupId);
    for(key in doc.passwords) {
      users.push(key)
    }
    return users;
  }
}
