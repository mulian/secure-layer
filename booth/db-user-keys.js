// import {dbTasks} from './db.js'
import {schemaUserKeys,objectSize} from '../servers/simple-schemas.js'

export default
class DBUserKeys extends Mongo.Collection {
  constructor() {
    super('secure_layer_user',{
      transform: function(doc) {
        if(Meteor.user())
          if ((Meteor.isClient) && (Meteor.userId()==doc._id)) {
            doc.privateKey = SecureLayer.crypt.AES.dec(doc.privateKey,SecureLayer.user.getPassword());
            doc.encrypted = false;
          }
        return doc;
      },
    });
    this.created = false;
    // schemaUserKeys.attachSchema(this);
    if(Meteor.isServer)
      this.allow({
        insert: function(userId,doc) {
          return doc._id == userId && Match.test(doc,schemaUserKeys);
        },
        update: function(userId,doc,fields,modifier) {
          // console.log(doc);
          return false;
          if(userId==doc._id && modifier) {
            if(objectSize(modifier)==1 && modifier['$set']) {
              return doc._id == userId;
            }
          }
          return false;
        },
        remove: function(userId,doc) {
          if(userId==doc._id) return true;
          return false;
        },
      });
  }

  /*
  * Get PublicKey from User Id
  * @param {string} userId default own Meteor User Id
  * @return {object} return publicKey from user or undefined if no user with id found
  */
  getPublicKey(userId=Meteor.userId()) {
    SecureLayer.user.isLoggedIn();
    let obj = this.findOne({_id:userId});
    if(obj!=undefined) return obj.publicKey;
    else return undefined;
  }
  /*
  * Get my own Private Key
  * @return {object} return privateKey if there is one decrypted
  */
  getPrivateKey() {
    SecureLayer.user.isLoggedIn();
    if(this.privateKey==undefined) {
      let obj = this.findOne({_id:Meteor.userId()});
      if(obj.encrypted==false) this.privateKey = obj.privateKey;
    }

    return this.privateKey;
  }

  // insert(...args) {
  //   super.insert.apply(this,args);
  // }

  /*
  * Create new User, have to call after create User with Meteor.
  * It will create the asymmetric key and encrypt the privateKey with user Password Hash.
  * @param {string} userId default own Meteor User Id
  */
  create(callback) {
    SecureLayer.user.isLoggedIn();
    let asyncKeys= SecureLayer.crypt.RSA.createAsyncKeys();
    this.insert({
      _id: Meteor.userId(),
      publicKey: asyncKeys.public,
      privateKey: SecureLayer.crypt.AES.enc(asyncKeys.private, SecureLayer.user.getPassword()),
      encrypted: true,
    },callback);
  }
  /* ChangePassword (not good tested)
  */
  changePassword(newPassword) {
    SecureLayer.user.isLoggedIn();
    let plainPrivateKey = this.getPrivateKey();
    SecureLayer.user.setPassword(newPassword);
    this.update({_id:eteor.userId()},{$set:{
      privateKey: SecureLayer.crypt.AES.enc(plainPrivateKey, SecureLayer.user.getPassword())
    }});
  }

  //TODO: new RSA passwords....


  //TODO: remove insert,update,delete from here?
}
