import {dbUserKeys,dbGroupsUsers,addCollection} from '../clients/secure-layer.js'
import CollectionDecrypted from '../clients/collection-decrypted.js'

export default
class DBSecureCollection extends Mongo.Collection {
  /* Constructor
  * - Set Mongo.Collection super, decrypt before call transform
  * - Add class Attribute secureVars
  * - Init "local" Collection with localTransform
  *
  * @param {string} name of Mongo Collection, same as Mongo.Collection
  * @param {array} secureVars attributes how secured with groupId - PW
  * @param {object} options same as Mongo.Collection. Plus localTransform.
  * @param {function} options.localTransform for transform the local Collection
  */
  constructor(name,secureVars,options={}) {
    //copy original transform to afterDecryptTransform
    if(secureVars!=false) {
      let afterDecryptTransform = options.transform;
      if(Meteor.isClient) {
        options.transform = (doc) => {
          doc = this.decryptTransform(doc);
          if(afterDecryptTransform) doc = afterDecryptTransform(doc);
          return doc;
        };
      }
    }
    super(name,options);
    if(secureVars!=false) {
      // this.setPermissions(); //TODO: Without option
      this.setSecureVars(secureVars);
      this.localTransform = options.localTransform;
      addCollection(this);
      if(Meteor.isClient) {
        this.local = new CollectionDecrypted(this,{transform:this.localTransform});
      }
    } else this.secureVars = false;

  }
  setPermissions() {
    this.allow({
      insert: function(userId,doc) {
        return true;
        // return (userId && _.contains(doc.users,userId));
      },
      update: function(userId,doc,fields,modifier) {
        return true;
        // return (userId && _.contains(doc.users,userId));
      },
      remove: function(userId,doc) {
        return true;
        // return (userId && _.contains(doc.users,userId));
      },
      fetch: ['owner','users'],
    });
  }

  setSecureVars(secureVars) {
    if(secureVars instanceof Array) {
      for(item of secureVars) {
        if(item=='_id' || item=='encrypted' || item=='groupId' || item=='users')
          throw "DBSecureCollection: do not use unvalid secureVars";
      }
      this.secureVars = secureVars;
    } else throw "DBSecureCollection: no secureVars set for Collection "+this._name;
  }
  /* DecryptTransform
  * Decrypt doc's secure Variables before transform.
  *
  * @param {object} doc
  * @return {object} doc
  */
  decryptTransform(doc) {
    if(doc.encrypted) {
      let groupDoc = dbGroupsUsers.findOne({_id:doc.groupId});
      // console.log(groupDoc);
      if(groupDoc) {
        for(item of this.secureVars) {
          if(doc[item]) {
            doc[item] = SecureLayer.crypt.AES.dec(doc[item],groupDoc.password);
            if(doc[item]==null) {
              throw new Error('Could not decrypt Doc.');
              return doc; //could not decrypt
            }
          }
        }
        doc.encrypted=false;
      }
    }
    // console.log(doc);
    return doc;
  }
  /* private Encrypt doc's secure Variables befor update/insert
  *
  * @param {object} doc
  * @param {string} password of group
  * @return {object} doc
  */
  _encrypt(doc,password) {
    for(item of this.secureVars) {
      if(doc[item]) {
        doc[item] = SecureLayer.crypt.AES.enc(doc[item],password);
        doc.encrypted = true;
      }
    }
    return doc;
  }

  /* Alter Users
  *
  * @param {string} groupId
  * @param {array} newUsers ids
  */
  alterUsers(groupId,newUsers,cb) {
    return dbGroupsUsers.alterUsers(groupId,newUsers,cb);

    // let affected = this.find({groupId:groupId}).fetch();
    // for(doc of affected) {
    //   this.update({_id:doc._id},{$set:{users:newUsers}});
    // }
  }
  /* Alter Users
  *
  * @param {string} groupId
  * @param {array} newUsers ids
  */
  alterAdmins(groupId,newUsers,cb) {
    dbGroupsUsers.alterAdmins(groupId,newUsers,cb);

    // let affected = this.find({groupId:groupId}).fetch();
    // for(doc of affected) {
    //   this.update({_id:doc._id},{$set:{users:newUsers}});
    // }
  }

  /* Create new Group
  * ... with users
  * @param {array} users list
  * @param {object} options
  * @param {function} options.callback callback after insert
  * @param {string} option.type type of group
  * @return {string} groupId
  */
  createGroup(users,option={}) {
    //TODO: +es dürfen nur bestimmte subUsers die gruppe ändern.
    return dbGroupsUsers.create(users,option);
  }
  getGroups() {
    // query.groupUsers = Meteor.userId();
    return dbGroupsUsers;
  }

  /* Insert doc
  *
  * @param {object} doc mongo collection doc
  * @param {string} doc.groupId in secure collection necessary (pre use createGroup);
  * @param rest args will transmit to super
  * @return
  */
  insert(...args) {
    let doc = args[0];
    if(this.secureVars==false) {
      console.log("INSERT",args);
      return super.insert.apply(this,args);
    } else {
      if(doc.groupId && Meteor.isClient) {
        let groupObj = dbGroupsUsers.findOne({_id:doc.groupId});
        // let groupMembers = dbGroupsUsers.getGroupeUsers(groupObj);
        if(groupObj && groupObj.password) {
          doc = this._encrypt(doc,groupObj.password);
          doc.groupUsers = groupObj.users;
          doc.groupAdmins = [Meteor.userId()];
          return super.insert.apply(this,args);
        } else throw new Error('doc.groupId must be valid');
      } else throw new Error('doc.groupId must be set');
    }
  }

  update(...args) {
    let query=args[0];
    let modifier=args[1];
    if(Meteor.isClient && (this.secureVars!=false)) {
      let currentDoc = this.findOne(query);
      if(currentDoc) {
        let docGroupId = currentDoc.groupId;
        if(modifier.$set && modifier.$set.groupId) docGroupId = modifier.$set.groupId;
        let groupDoc = dbGroupsUsers.findOne({_id:docGroupId});
        if(groupDoc && groupDoc.password!=null && currentDoc) {
          if(modifier['$set']) modifier['$set'] = this._encrypt(modifier['$set'],groupDoc.password);
          else modifier = this._encrypt(modifier,groupDoc.password);
        } else {
          throw new Error('There is no Secure Group.');
          return null;
        }
      } else {
        throw new Error('There is no Doc to update.');
        return null;
      }
    }
    // args.unshift(query,modifier);
    return super.update.apply(this,args);
  }
}
