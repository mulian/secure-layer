Package.describe({
  name: 'mulian:secure-layer',
  version: '0.0.6',
  // Brief, one-line summary of the package.
  summary: 'SecureLayer for Home',
  // URL to the Git repository containing the source code for this package.
  git: 'https://github.com/mulian/secure-layer',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.4.2.3');
  api.use('ecmascript');

  api.use('check');
  api.use('mongo');
  //
  api.use('aldeed:simple-schema@1.5.3');
  api.use('jparker:crypto-md5@0.1.1');
  api.use('jparker:crypto-aes@0.1.0');
  api.use('check@1.2.4');
  api.use('jayuda:flx-jsencrypt@0.0.9');
  api.use('accounts-password');
  api.use('underscore');
  // api.use('cfs:gridfs');

  // api.use('grigio:babel');

  // api.addFiles([
  //   'booth/db-groups-users.js',
  //   'booth/db-secure-collection.js',
  //   'booth/db-user-keys.js'
  // ], ['client','server']);
  // api.addFiles([
  //   'server/simple-schemas.js',
  //   'server/rules/rules-basic.js'
  // ], 'server');
  // api.addFiles([
  //   'client/collection-decrypted.js',
  //   'client/secure-layer.js'
  // ], 'client');
  api.mainModule('clients/secure-layer.js');
  // api.mainModule('./test.js');
  api.export('SecureLayer');
  // api.export('DBSecureCollection','client');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('mulian:secure-layer');
  api.use('accounts-password');
  api.use('underscore');
  // api.mainModule('test/secure-layer-tests.js');
  api.mainModule('test/secure-layer.test.js');

  api.use('practicalmeteor:mocha');
  // api.addFiles([
  //   'test/secure-collection-test.js',
  //   'test/secure-layer-test-tools.js',
  //   'test/secure-layer-tests.js',
  //   'test/secure-collection-test.js',
  // ]);//...
});
